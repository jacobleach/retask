package retask

import (
	"encoding/json"
	"strconv"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
)

func TestNewTask(t *testing.T) {
	task := NewTask("foo")

	assert.NotEqual(t, task.ID(), uuid.Nil.String(), "ID should not be nil")
	assert.Equal(t, task.Name, "foo", "Name should match what was passed")
	assert.Nil(t, task.Expiration(), "Expiration should be nil")
	assert.Equal(t, task.Retry(), 0, "Retry count should be zero")
	assert.Equal(t, task.MaxRetries(), DefaultMaxRetries, "Max retries equals default value")
}

func TestJSON(t *testing.T) {
	now := time.Now()
	task := NewTask("foo").
		SetMaxRetries(7).
		SetExpiration(now.Add(5 * time.Second))

	b, err := json.Marshal(task)
	assert.NoError(t, err)

	tests := []struct {
		key   string
		value string
	}{
		{key: "id", value: task.id.String()},
		{key: "name", value: task.Name},
		{key: "expiration", value: task.expiration.Format(time.RFC3339Nano)},
		{key: "retry", value: "0"},
		{key: "maxRetries", value: "7"},
	}

	// Unmarshal using _not_ our Unmarshal to validate our Marshal
	jsonMap := make(map[string]json.RawMessage)
	json.Unmarshal(b, &jsonMap)

	for _, tc := range tests {
		v, err := strconv.Unquote(string(jsonMap[tc.key]))
		// Didn't have quotes, use value directly
		if err != nil {
			assert.Equal(t, tc.value, string(jsonMap[tc.key]), tc.key)
		} else {
			assert.Equal(t, tc.value, v, tc.key)
		}
	}

	// Now Unmarshal using our Unmarshal to test it
	var jsonTask Task
	assert.NoError(t, json.Unmarshal(b, &jsonTask))

	fixTime(&jsonTask)
	fixTime(task)

	assert.Equal(t, task, &jsonTask)
}

func TestSetTTL(t *testing.T) {
	now := time.Now()
	timeNow = func() time.Time {
		return now
	}

	task := NewTask("foo").SetTTL(250 * time.Millisecond)
	assert.Equal(t, *task.Expiration(), now.Add(250*time.Millisecond))
}
