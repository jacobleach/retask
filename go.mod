module gitlab.com/jacobleach/retask

// +heroku goVersion go1.15
go 1.15

require (
	github.com/alicebob/miniredis/v2 v2.14.3
	github.com/go-redis/redis/v8 v8.8.0
	github.com/google/uuid v1.2.0
	github.com/onsi/ginkgo v1.15.2 // indirect
	github.com/onsi/gomega v1.11.0 // indirect
	github.com/stretchr/testify v1.7.0
)
