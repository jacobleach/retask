package main

import (
	"context"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/go-redis/redis/v8"
	"gitlab.com/jacobleach/retask"
)

func main() {
	c := retask.NewClient(&redis.Options{
		Addr: "localhost:6379",
	})

	if err := c.Add(retask.NewTask("foo").SetMaxRetries(5).SetTTL(5 * time.Second)); err != nil {
		log.Fatal(err)
	}

	consumer := retask.NewConsumer(&redis.Options{
		Addr: "localhost:6379",
	})

	consumer.Register("foo", func(ctx context.Context, task *retask.Task) error {
		time.Sleep(4 * time.Second)

		if rand.Intn(2) > 0 {
			fmt.Println("Task failed!")
			return errors.New("Task failed")
		}

		fmt.Println("Task succeeded!")

		return nil
	})

	go func() {
		for {
			tasks, _ := c.Processing()
			fmt.Println("Tasks currently processing:", tasks)
			time.Sleep(1 * time.Second)
		}
	}()

	consumer.Consume()
}
