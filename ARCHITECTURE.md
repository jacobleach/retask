Architecture
------------

The `retask` architecture fits into two large concerns: the data model in redis, and the golang library.

Redis Data Model
----------------

* A queue is represented as a [list](https://redis.io/topics/data-types#lists) in redis.  Today, we allow one queue with the redis key `queue`.
* A task is represented on the queue as its taskId.
* A task is represented in redis as the key `task-<id>`, with the JSON task struct defined in [retask.go](retask.go)
** Custom data on the task is stored seperately in the key `data-<id>`.
* When work is taken from the queue, we expect it to be atomically removed from the queue and placed into a `processing` list, where it can be monitored.
* When a task is finished it will be moved to either the `complete` or `failed` list depending on its status.

Golang Library
--------------

* The library expects us to queue items to the left (head) and pop from the right (tail) from the `queue` key in redis.
