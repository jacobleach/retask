package retask

import (
	"encoding/json"
	"time"

	"github.com/google/uuid"
)

var DefaultMaxRetries = 5

// Support unit testing
var timeNow = time.Now

type Task struct {
	Name string
	Data []byte

	id uuid.UUID

	expiration *time.Time

	maxRetries int
	retry      int
}

// taskMsg is an internal type to enable marshaling of internal fields
type taskMsg struct {
	ID         uuid.UUID  `json:"id"`
	Name       string     `json:"name"`
	Expiration *time.Time `json:"expiration"`
	MaxRetries int        `json:"maxRetries"`
	Retry      int        `json:"retry"`
}

func (t *Task) MarshalJSON() ([]byte, error) {
	return json.Marshal(&taskMsg{
		Name:       t.Name,
		ID:         t.id,
		Expiration: t.expiration,
		MaxRetries: t.maxRetries,
		Retry:      t.retry,
	})
}

func (t *Task) UnmarshalJSON(data []byte) error {
	v := taskMsg{}
	if err := json.Unmarshal(data, &v); err != nil {
		return err
	}

	t.id = v.ID
	t.Name = v.Name
	t.expiration = v.Expiration
	t.maxRetries = v.MaxRetries
	t.retry = v.Retry

	return nil
}

func NewTask(name string) *Task {
	return &Task{
		Name:       name,
		id:         uuid.New(),
		maxRetries: DefaultMaxRetries,
	}
}

func (t *Task) ID() string {
	return t.id.String()
}

func (t *Task) Retry() int {
	return t.retry
}

func (t *Task) MaxRetries() int {
	return t.maxRetries
}

func (t *Task) Expiration() *time.Time {
	return t.expiration
}

func (t *Task) SetMaxRetries(maxRetries int) *Task {
	t.maxRetries = maxRetries

	return t
}

func (t *Task) SetExpiration(expiration time.Time) *Task {
	t.expiration = &expiration

	return t
}

func (t *Task) SetTTL(ttl time.Duration) *Task {
	v := timeNow().Add(ttl)
	t.expiration = &v

	return t
}
