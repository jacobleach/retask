package retask

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"

	"github.com/go-redis/redis/v8"
)

const queueKey = "queue"
const processingKey = "processing"
const failedKey = "failed"
const completeKey = "complete"

const taskKey = "task"
const dataKey = "data"

// KEYS[1] -> taskKey
// KEYS[2] -> dataKey
const getTaskCmd = `
local task = redis.call('GET', KEYS[1])
local data = redis.call('GET', KEYS[2])
return {task, data}
`

// KEYS[1] -> taskKey
// KEYS[2] -> dataKey
// ARGV[1] -> task
// ARGV[2] -> data
const setTaskCmd = `
redis.call('SET', KEYS[1], ARGV[1])
redis.call('SET', KEYS[2], ARGV[2])
return redis.status_reply("OK")
`

const requeueCmd = `
if redis.call('LREM', KEYS[1], 0, ARGV[1]) == 0 then
	return redis.error_reply("Item not found")
end
redis.call('RPUSH', KEYS[2], ARGV[1])
return redis.status_reply("OK")
`

// KEYS[1] -> queue
// KEYS[2] -> task
// KEYS[3] -> data
// ARGV[1] -> id
// ARGV[2] -> task
// ARGV[2] -> data
const queueCmd = `
redis.call('LPUSH', KEYS[1], ARGV[1])
redis.call('SET', KEYS[2], ARGV[2])
redis.call('SET', KEYS[3], ARGV[3])
return redis.status_reply("OK")
`

type Client struct {
	redisClient *redis.Client
}

func NewClient(opts *redis.Options) *Client {
	return &Client{
		redisClient: redis.NewClient(opts),
	}
}

func makeKey(key string, id string) string {
	return fmt.Sprintf("%s-%s", key, id)
}

func (c *Client) Add(task *Task) error {
	b, err := json.Marshal(&task)
	if err != nil {
		return err
	}

	return redis.NewScript(queueCmd).Run(
		context.Background(),
		c.redisClient,
		[]string{queueKey, makeKey(taskKey, task.id.String()), makeKey(dataKey, task.id.String())},
		task.id.String(),
		b,
		task.Data,
	).Err()
}

func (c *Client) Update(ctx context.Context, task *Task) error {
	b, err := json.Marshal(&task)
	if err != nil {
		return err
	}

	return redis.NewScript(setTaskCmd).Run(
		context.Background(),
		c.redisClient,
		[]string{makeKey(taskKey, task.id.String()), makeKey(dataKey, task.id.String())},
		b,
		task.Data,
	).Err()
}

func (c *Client) consume() (*Task, error) {
	value, err := c.redisClient.BRPopLPush(context.Background(), queueKey, processingKey, 0).Result()
	if err != nil {
		return nil, err
	}

	return c.getTask(value)
}

func (c *Client) getTask(id string) (*Task, error) {
	result, err := redis.NewScript(getTaskCmd).Run(
		context.Background(),
		c.redisClient,
		[]string{makeKey(taskKey, id), makeKey(dataKey, id)},
	).Result()

	if err != nil {
		return nil, err
	}

	values, ok := result.([]interface{})
	if !ok {
		return nil, errors.New("Failed to cast result to `[]interface{}`")
	}

	var task Task
	if err := json.Unmarshal([]byte(values[0].(string)), &task); err != nil {
		return nil, err
	}

	data := []byte(values[1].(string))
	if !ok {
		return nil, errors.New("Failed to cast value to `string`")
	}

	if len(data) > 0 {
		task.Data = data
	}

	return &task, nil
}

func (c *Client) requeue(task *Task) error {
	return redis.NewScript(requeueCmd).Run(
		context.Background(),
		c.redisClient,
		[]string{processingKey, queueKey},
		task.id.String(),
	).Err()
}

// KEYS[1] -> processing key
// KEYS[1] -> complete key
// ARGV[1] -> id
const completeTaskScript = `
if redis.call('LREM', KEYS[1], 0, ARGV[1]) == 0 then
	return redis.error_reply("Item not found")
end
redis.call('LPUSH', KEYS[2], ARGV[1])
return redis.status_reply("OK")
`

func (c *Client) completeTask(task *Task) error {
	return redis.NewScript(completeTaskScript).Run(
		context.Background(),
		c.redisClient,
		[]string{processingKey, completeKey},
		task.id.String(),
	).Err()
}

func (c *Client) failTask(task *Task) error {
	return redis.NewScript(completeTaskScript).Run(
		context.Background(),
		c.redisClient,
		[]string{processingKey, failedKey},
		task.id.String(),
	).Err()
}

func (c *Client) Processing() ([]string, error) {
	return c.redisClient.LRange(context.Background(), processingKey, 0, -1).Result()
}

func (c *Client) Failed() ([]string, error) {
	return c.redisClient.LRange(context.Background(), failedKey, 0, -1).Result()
}

func (c *Client) Completed() ([]string, error) {
	return c.redisClient.LRange(context.Background(), completeKey, 0, -1).Result()
}
