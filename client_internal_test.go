package retask

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/alicebob/miniredis/v2"
	"github.com/go-redis/redis/v8"
	"github.com/stretchr/testify/suite"
)

func TestMockedRedisTestSuite(t *testing.T) {
	suite.Run(t, new(MockedRedisTestSuite))
}

type MockedRedisTestSuite struct {
	suite.Suite
	client *Client
	redis  *miniredis.Miniredis
}

func (s *MockedRedisTestSuite) SetupTest() {
	r, err := miniredis.Run()

	s.Require().NoError(err)

	c := NewClient(&redis.Options{
		Addr: r.Addr(),
	})

	s.client = c
	s.redis = r
}

func (s *MockedRedisTestSuite) AfterTest() {
	s.redis.Close()
}

// time.Time has an internal clock reading that is different. Rounding to 0 makes the equality
// check work correctly for that. If one of the times is UTC the location for that time is nil.
// Setting both to UTC makes both locations nil so the comparison works.
func fixTime(task *Task) *Task {
	*task.expiration = task.expiration.Round(0).UTC()

	return task
}

func (s *MockedRedisTestSuite) TestAdd() {
	task := NewTask("foo").SetTTL(5 * time.Second).SetMaxRetries(5)
	s.Require().NoError(s.client.Add(task))

	s.Run("Ensure saved task is correct", func() {
		value, err := s.client.redisClient.Get(context.Background(), fmt.Sprintf("task-%s", task.ID())).Result()
		s.Require().NoError(err)

		var redisTask Task
		s.Require().NoError(json.Unmarshal([]byte(value), &redisTask))

		s.Assert().Equal(fixTime(task), fixTime(&redisTask))
	})

	s.Run("Queue should only have one item", func() {
		value, err := s.client.redisClient.LLen(context.Background(), "queue").Result()
		s.Require().NoError(err)

		s.Assert().EqualValues(1, value)
	})

	s.Run("ID in Queue should be correct", func() {
		value, err := s.client.redisClient.LRange(context.Background(), "queue", 0, 1).Result()
		s.Require().NoError(err)

		s.Assert().Equal(task.ID(), value[0])
	})
}

func (s *MockedRedisTestSuite) TestUpdate() {
	task := NewTask("foo").SetTTL(5 * time.Second).SetMaxRetries(5)
	s.Require().NoError(s.client.Add(task))

	updatedTask := *task
	updatedTask.Data = []byte("foo")

	s.Require().NoError(s.client.Update(context.Background(), &updatedTask))
	redisTask, err := s.client.getTask(task.id.String())
	s.Require().NoError(err)

	s.Assert().Equal(fixTime(&updatedTask), fixTime(redisTask), "Ensure saved task is correct")
}

func (s *MockedRedisTestSuite) TestConsume() {
	task := NewTask("foo").SetTTL(5 * time.Second).SetMaxRetries(5)
	s.Require().NoError(s.client.Add(task))

	redisTask, err := s.client.consume()
	s.Require().NoError(err)

	s.Assert().Equal(fixTime(task), fixTime(redisTask), "Ensure returned task is correct")

	result, err := s.client.redisClient.LRange(context.Background(), queueKey, 0, -1).Result()
	s.Require().NoError(err)
	s.Assert().NotSubset(result, []string{task.ID()})

	result, err = s.client.redisClient.LRange(context.Background(), processingKey, 0, -1).Result()
	s.Require().NoError(err)
	s.Assert().Subset(result, []string{task.ID()})
}

func (s *MockedRedisTestSuite) TestCompleteTask() {
	s.Require().NoError(s.client.Add(NewTask("foo")))

	task, err := s.client.consume()
	s.Require().NoError(err)

	s.Require().NoError(s.client.completeTask(task))

	result, err := s.client.redisClient.LRange(context.Background(), processingKey, 0, -1).Result()
	s.Require().NoError(err)
	s.Assert().NotSubset(result, []string{task.ID()})

	result, err = s.client.redisClient.LRange(context.Background(), completeKey, 0, -1).Result()
	s.Require().NoError(err)
	s.Assert().Subset(result, []string{task.ID()})
}

func (s *MockedRedisTestSuite) TestFailTask() {
	s.Require().NoError(s.client.Add(NewTask("foo")))

	task, err := s.client.consume()
	s.Require().NoError(err)

	s.Require().NoError(s.client.failTask(task))

	result, err := s.client.redisClient.LRange(context.Background(), processingKey, 0, -1).Result()
	s.Require().NoError(err)
	s.Assert().NotSubset(result, []string{task.ID()})

	result, err = s.client.redisClient.LRange(context.Background(), failedKey, 0, -1).Result()
	s.Require().NoError(err)
	s.Assert().Subset(result, []string{task.ID()})
}

func (s *MockedRedisTestSuite) TestCompleted() {
	s.Require().NoError(s.client.Add(NewTask("foo")))

	task, err := s.client.consume()
	s.Require().NoError(err)
	s.Require().NoError(s.client.completeTask(task))

	result, err := s.client.redisClient.LRange(context.Background(), completeKey, 0, -1).Result()
	s.Require().NoError(err)
	s.Assert().Subset(result, []string{task.ID()})

	s.Require().NoError(s.client.Add(NewTask("foo")))

	task2, err := s.client.consume()
	s.Require().NoError(err)
	s.Require().NoError(s.client.completeTask(task2))

	result, err = s.client.redisClient.LRange(context.Background(), completeKey, 0, -1).Result()
	s.Require().NoError(err)
	s.Assert().Subset(result, []string{task.ID(), task2.ID()})
}

func (s *MockedRedisTestSuite) TestFailed() {
	s.Require().NoError(s.client.Add(NewTask("foo")))

	task, err := s.client.consume()
	s.Require().NoError(err)
	s.Require().NoError(s.client.failTask(task))

	result, err := s.client.redisClient.LRange(context.Background(), failedKey, 0, -1).Result()
	s.Require().NoError(err)
	s.Assert().Subset(result, []string{task.ID()})

	s.Require().NoError(s.client.Add(NewTask("foo")))

	task2, err := s.client.consume()
	s.Require().NoError(err)
	s.Require().NoError(s.client.failTask(task2))

	result, err = s.client.redisClient.LRange(context.Background(), failedKey, 0, -1).Result()
	s.Require().NoError(err)
	s.Assert().Subset(result, []string{task.ID(), task2.ID()})
}
