package retask

import (
	"context"
	"fmt"
	"os"

	"github.com/go-redis/redis/v8"
)

type Handler func(ctx context.Context, task *Task) error

type Consumer struct {
	client   *Client
	handlers map[string]Handler
}

func NewConsumer(opts *redis.Options) *Consumer {
	return &Consumer{
		client:   NewClient(opts),
		handlers: make(map[string]Handler),
	}
}

func (c *Consumer) Register(name string, handler Handler) {
	c.handlers[name] = handler
}

func (c *Consumer) Consume() {
	for {
		task, err := c.client.consume()
		if err != nil {
			panic(err)
		}

		runHandler(c.client, c.handlers[task.Name], task)
	}
}

func runHandler(client *Client, handler Handler, task *Task) {
	go func() {
		ctx, cancel := context.WithDeadline(context.Background(), *task.expiration)
		defer cancel()

		select {
		case <-ctx.Done():
			fmt.Println("Task expired before execution")

			if err := client.completeTask(task); err != nil {
				fmt.Fprintln(os.Stderr, "Failed to complete task after expiration:", err)
			}

			return
		default:
		}

		err := handler(ctx, task)

		select {
		case <-ctx.Done():
			fmt.Println("Task expired after execution")

			if err := client.completeTask(task); err != nil {
				fmt.Fprintln(os.Stderr, "Failed to complete task after expiration:", err)
			}

			return
		default:
		}

		// nolint:nestif
		if err != nil {
			if task.retry < task.maxRetries {
				task.retry++
				if err := client.Update(ctx, task); err != nil {
					fmt.Fprintf(os.Stderr, "Failed to update task retries: %v\n", err)
				}
			} else {
				fmt.Println("Task ran out of retries")

				if err := client.failTask(task); err != nil {
					fmt.Fprintf(os.Stderr, "Failed to mark task complete: %v\n", err)
				}

				return
			}

			fmt.Println("Retrying task")

			if err := client.requeue(task); err != nil {
				fmt.Fprintf(os.Stderr, "Failed to requeue failed task: %v\n", err)
			}

			return
		}

		if err := client.completeTask(task); err != nil {
			fmt.Fprintf(os.Stderr, "Failed to mark task complete: %v\n", err)
		}
	}()
}
